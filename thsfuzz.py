from lxml import html
import re
import requests
import socket
import argparse
import time
import sys
import itertools


def udp_fuzz(args, bufferinc, buffersize):
	
        
        if TYPE is None:
                print '[!] Error: You must supply a packet type of tcp or udp'
                sys.exit()
        if HOST is None:
                print '[!] Error: You must supply the target ip'
                sys.exit()
        if PORT is None:
                print '[!] Error: You must supply the target port'
                sys.exit()
        if DATA is None:
                print '[!] Error: You must supply a file list'
                sys.exit()


	print "[*] The Following Parameters Have Been Selected, Press CTRL C To Cancel.."
        print ""
        print "[*] [ "+str(TYPE)+" ]"+" ["+str(HOST)+"] "+" ["+str(PORT)+"] "+" ["+str(DATA)+"] "

        time.sleep(2)

	with open(DATA) as f:
		line = f.readlines()
		for s in line:
			fuzz = s.strip()
			buffer = (fuzz)* buffersize
			print '[*] Sending Buffer Size of '+str(buffersize)
			client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			try:
				client.connect((HOST,int(PORT)))
				client.send(buffer)
				client.close()
				buffersize = buffersize+bufferinc
				time.sleep(.2)
			except:
				raw_input('[*] Port Might Be Closed...Press Enter To Continue')
	udp_fuzz(args, buffersize, buffersize)
	
def tcp_fuzz(args, bufferinc, buffersize):


        if TYPE is None:
                print '[!] Error: You must supply a packet type of tcp or udp'
                sys.exit()
        if HOST is None:
                print '[!] Error: You must supply the target ip'
                sys.exit()
        if PORT is None:
                print '[!] Error: You must supply the target port'
                sys.exit()
        if DATA is None:
                print '[!] Error: You must supply a file list'
                sys.exit()

	
        print "[*] The Following Parameters Have Been Selected, Press CTRL C To Cancel.."
        print ""
        print "[*] [ "+str(TYPE)+" ]"+" ["+str(HOST)+"] "+" ["+str(PORT)+"] "+" ["+str(DATA)+"] "

        time.sleep(2)

        with open(DATA, 'rb') as f:
                line = f.readlines()
		for s in line:
                        fuzz = s.strip()
                        buffer = (fuzz)* buffersize
			print '[*] Sending Buffer Size of '+str(buffersize)
                        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			try:
                                client.connect((HOST,int(PORT)))
				client.send(buffer)
				client.close()
				buffersize = buffersize+bufferinc
				time.sleep(.2)
				
			except:
				raw_input('[*] Port Might Be Closed...Press Enter To Continue')

	tcp_fuzz(args, bufferinc, buffersize)
		

def url_fuzz(args):

        HTTP = 'http://www.'+DOMAIN
	HTTPS = 'https://www.'+DOMAIN

	print "[*] The Following Parameters Have Been Selected, Press CTRL C To Cancel.."
        print ""
        print "[*] [ "+str(TYPE)+" ]"+" ["+str(DOMAIN)+"]"

        time.sleep(2)
	
	print HTTP
	print HTTPS

	rg = requests.get(HTTP)
	rh = requests.head(HTTP)
	rp = requests.put(HTTP)
	rd = requests.delete(HTTP)
	rs = requests.post(HTTP)

	print '[*] Checking Requests Methods..'
	
	print '[GET]   : '+DOMAIN+' ['+str(rg.status_code)+']'	
	print '[HEAD]  : '+DOMAIN+' ['+str(rh.status_code)+']'
	print '[PUT]   : '+DOMAIN+' ['+str(rp.status_code)+']'
        print '[DELETE]: '+DOMAIN+' ['+str(rh.status_code)+']'
        print '[POST]  : '+DOMAIN+' ['+str(rp.status_code)+']'

	print '[*] Checking for robots.txt...'

	robots = requests.get(HTTP+'/robots.txt')
	if robots.status_code == 200:
		print '[*] robots.txt file found... saved...'
		with open(DOMAIN+'.robots', 'a') as f:
			f.write(robots.text)
			f.close()

	data = rg.text
	webpage = html.fromstring(rg.content)
	linklist = webpage.xpath('//a/@href')
	for item in linklist:
		if item.find('http') != -1 and item.find(DOMAIN) != -1:
			with open(DOMAIN+'.links', 'a') as f:
	                       	f.write(item)
	                      	f.write('\n')
				f.close()

	with open(DOMAIN+'.links', 'r') as f:
		for line in f:
			httpline = line.split("http://")
			httpsline = line.split("https://")

			if DOMAIN in httpline[0] or DOMAIN in httpsline[0]:
				print line

		#	if str(domline[0]) == str(HTTP) or str(domline[0]) == str(HTTPS):
		#		print domline[0] 
		
	

if __name__ == '__main__':
	
	print '''

	O~~~ O~~~~~~~~ O~~     O~~  O~~ ~~  O~~~~~~~~O~~     O~~O~~~~~~~ O~~O~~~~~~~ O~~     
     		O~~    O~~     O~~O~~    O~~O~~      O~~     O~~       O~~         O~~       
     		O~~    O~~     O~~ O~~      O~~      O~~     O~~      O~~         O~~        
     		O~~    O~~~~~~ O~~   O~~    O~~~~~~  O~~     O~~    O~~         O~~          
     		O~~    O~~     O~~      O~~ O~~      O~~     O~~   O~~         O~~           
     		O~~    O~~     O~~O~~    O~~O~~      O~~     O~~ O~~         O~~             
     		O~~    O~~     O~~  O~~ ~~  O~~        O~~~~~   O~~~~~~~~~~~O~~~~~~~~~~~  

		[-------   A TCP/UDP & WEB fuzzer -R4v3N // www.top-hat-sec.com  --------]
		[   -t (tcp or udp) -i (ip address) -p (port) -f (file fuzz list)  ]
	
	'''

	parser = argparse.ArgumentParser(description='A Custom TCP/UDP Fuzzer -R4v3N')
	parser.add_argument('-m','--packetmode', help='Type TCP, UDP, or WEB',)
	parser.add_argument('-i','--ip', help='Target IP',)
	parser.add_argument('-p','--port', help='Target Port',)
	parser.add_argument('-f','--file', help='Fuzzer List',)
	parser.add_argument('-b', '--buffer', help='Fuzzer Buffer Increment Size', default=1, required=False)
	parser.add_argument('-d', '--domain', help='Web Domain to fuzz', required=False)
	args = parser.parse_args()
	
	TYPE = args.packetmode
	HOST = args.ip
	PORT = args.port
	DATA = args.file
	DOMAIN = args.domain
	bufferinc = int(args.buffer)
	buffersize = 25	

        if TYPE is None:
                print '[!] Error: You must supply a packet type of tcp, udp, or url'
                sys.exit()
        
	time.sleep(2)

	if TYPE == 'tcp' or TYPE == 'TCP':

		tcp_fuzz(args, bufferinc, buffersize)
	else:
		pass

	if TYPE == 'udp' or TYPE == 'UDP':

		udp_fuzz(args, bufferinc, buffersize)
	else:
		pass
	if TYPE == 'web' or TYPE == 'WEB':

		url_fuzz(args)

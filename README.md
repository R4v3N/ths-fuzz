THS-FUZZ


THS-FUZZ is a python script that creates raw sockets over TCP or UDP and sends data using a buffer.
The same techniques are/were used doing buffer overflows in attempts to crash a service.
The user is able to set a buffer increment size which will increase the size each pass.
The user uses a wordlist of single characters (see payload.txt) which is multiplied and loaded into the buffer.
The buffer is then sent to the target IP and PORT using either TCP or UDP.
This process will continue until the port closes or your machine cannot handle the load.

**example:**

python thsfuzz.py -t tcp -i 192.168.1.11 -f /opt/ths-fuzz/payload.txt -p 10000 -b 2

* -t = protocol type (tcp or udp)
* -i = ip address of the target
* -f = the payload file
* -p = the target port number
* -b = the buffer increment size on each pass